
const express = require('express');
const { Pool } = require('pg');

const app = express();
const pool = new Pool({
  user: 'mentat',
  host: 'protective_postgresdb_1',
  database: 'mentat_events',
  password: 'mentat'
})

pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})

initDB();

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type, Access-Control-Allow-Origin, Accept");
  next();
});

app.post('/sendConfig', function (req, res) {
  pool.connect()
    .then(client => {
      return client.query('\
        INSERT INTO dash_config (userid, widget_conf, provider_conf) \
        VALUES ($1, $2, $3) \
        ON CONFLICT (userid) DO UPDATE SET \
        widget_conf = EXCLUDED.widget_conf, \
        provider_conf = EXCLUDED.provider_conf', [req.query.userId, JSON.parse(req.query.widgConf), JSON.parse(req.query.provConf)])
          .then(result => {            
            res.send(result);
            client.release()
          })
          .catch(err => {
            client.release()
            console.log(err.stack)
          })
      })
 });

app.get('/getConfig', function (req, res) {
  pool.connect()
    .then(client => {
      return client.query('SELECT * FROM dash_config WHERE userid = $1', [req.query.userId])
        .then(result => {          
          res.send(result);
          client.release()
        })
        .catch(err => {
          client.release()
          console.log(err.stack)
        })
    })
});

app.listen(4201, function () {
  console.log('Server listening to save/load protdash configurations');
});

function initDB(){
  pool.connect()
    .then(client => {
      return client.query("CREATE TABLE IF NOT EXISTS dash_config (userid text PRIMARY KEY, widget_conf jsonb, provider_conf jsonb)")
        .then(res => {
          client.release()
        })
        .catch(err => {
          client.release()
          console.log(err.stack)
    })
  })
}

